// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "Senha Segura Docs",
  tagline:
    "Welcome to Senha Segura's documentation! You can find all of our product's documentations here.",
  url: "http://senhasegura-test-1.s3-website-sa-east-1.amazonaws.com",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/faviSS.jpg",
  organizationName: "MT4", // Usually your GitHub org/user name.
  projectName: "docu-senhasegura", // Usually your repo name.

  presets: [
    [
      "@docusaurus/preset-classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          editUrl: "https://gitlab.com/odingroup/docu-senhasegura/-/tree/main",
        },
        // blog: {
        //   showReadingTime: true,
        //   // Please change this to your repo.
        //   editUrl:
        //     "https://github.com/facebook/docusaurus/edit/main/website/blog/",
        // },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      googleAnalytics: {
        trackingID: "UA-212397831-1",
        // Optional fields.
        anonymizeIP: true, // Should IPs be anonymized?
      },
      navbar: {
        // title: "My Site",
        logo: {
          alt: "My Site Logo",
          src: "img/logoSS.webp",
        },
        items: [
          {
            type: "docsVersionDropdown",
            docId: "intro",
            position: "left",
            label: "Tutorial",
          },
          {
            position: "right",
            type: "localeDropdown",
          },
          {
            to: "docs/Started/GetStarted",
            label: "Getting Stared",
            position: "left",
          },
          {
            type: "doc",
            docId: "Concepts/Overview",
            position: "left",
            label: "Documentation",
          },
          {
            to: "docs/api/api",
            position: "left",
            label: "API",
          },
          {
            to: "https://www.youtube.com/channel/UCpDms35l3tcrfb8kZSpeNYw",
            label: "Tutorials",
            position: "left",
          },
          // { to: "/blog", label: "Blog", position: "left" },
          {
            href: "https://www.senhasegura.com",
            label: "Website",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Docs",
            items: [
              {
                label: "Tutorial",
                to: "/docs/intro",
              },
            ],
          },
          {
            title: "Community",
            items: [
              // {
              //   label: "Stack Overflow",
              //   href: "https://stackoverflow.com/questions/tagged/docusaurus",
              // },
              // {
              //   label: "Discord",
              //   href: "https://discordapp.com/invite/docusaurus",
              // },
              {
                label: "Twitter",
                href: "https://twitter.com/marcusscharra",
              },
            ],
          },
          {
            title: "More",
            items: [
              // {
              //   label: "Blog",
              //   to: "/blog",
              // },
              {
                label: "GitLab",
                href: "https://gitlab.com/odingroup/docu-senhasegura/-/tree/main",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} SenhaSegura Docs. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
  plugins: [
    require.resolve("@cmfcmf/docusaurus-search-local"),
    // "@docusaurus/plugin-google-analytics",
  ],
};

module.exports = config;
