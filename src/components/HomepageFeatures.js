import React from "react";
import clsx from "clsx";
import styles from "./HomepageFeatures.module.css";

const FeatureList = [
  {
    title: "Certificate Management",
    source:
      "https://senhasegura.com/wp-content/uploads/2021/06/certificado.png",
    description: (
      <>
        Gerencie o ciclo completo de credenciais de alto privilégio a partir do
        controle de acesso com workflow e dupla custódia.
      </>
    ),
  },
  {
    title: "Gestão de Sessão (PSM)",
    source:
      "https://senhasegura.com/wp-content/uploads/2021/06/gravacao-de-sessao.png",
    description: (
      <>
        Conceda acesso aos servidores da sua organização sem revelar a senha,
        com gravação completa das sessões em texto e vídeo.
      </>
    ),
  },
  {
    title: "Gestão de Chaves",
    source:
      "https://senhasegura.com/wp-content/uploads/2021/06/gestao-de-chaves-ssh.png",
    description: (
      <>
        Controle o ciclo das chaves SSH desde a descoberta ou criação até a
        renovação ou revogação.
      </>
    ),
  },
  {
    title: "Provisionamento de Usuários Locais",
    source:
      "https://senhasegura.com/wp-content/uploads/2021/06/provisionamento-de-usuario.png",
    description: (
      <>
        Crie e gerencie usuários locais em servidores Linux, Unix e Windows de
        maneira centralizada.
      </>
    ),
  },
  {
    title: "Análise de Ameaças",
    source:
      "https://senhasegura.com/wp-content/uploads/2021/06/analise-de-ameacas.png",
    description: (
      <>
        Analise automaticamente ações críticas realizadas nos sistemas
        gerenciados e identifique operações cruciais.
      </>
    ),
  },
  {
    title: "Identidade de Aplicações",
    source:
      "https://senhasegura.com/wp-content/uploads/2021/06/cone-identidade-de-aplicacoes.png",
    description: (
      <>
        Acabe com as senhas hardcoded e identifique acessos privilegiados entre
        aplicações e aos bancos de dados.
      </>
    ),
  },
];

function Feature({ Svg, title, description, source }) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        {/* <Svg className={styles.featureSvg} alt={title} /> */}
        <img className={styles.featureSvg} alt={title} src={source}></img>
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
